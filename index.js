const btn = document.querySelector(".btn");
let themeCheck = "light";
btn.onclick = function themeChecker(){
    if (document.body.classList.contains("dark") === false){
        darkThemeSetter();
        themeCheck = "dark";
        localStorage.setItem("page_theme",themeCheck);
    }else{
        lightThemeSetter();
        themeCheck = "light";
        localStorage.setItem("page_theme",themeCheck);
    }
};
window.onload = function (){
    const theme = localStorage.getItem("page_theme");
    if (theme === "light"){
        lightThemeSetter();
    }else{
        darkThemeSetter();
    }
}
function lightThemeSetter() {
    document.body.classList.remove("dark");
    document.querySelector("article").classList.remove("dark");
    document.querySelector("h2").classList.remove("dark");
    document.querySelector(".last_line").classList.remove("dark");
}
function darkThemeSetter() {
    document.body.classList.add("dark");
    document.querySelector("article").classList.add("dark");
    document.querySelector("h2").classList.add("dark");
    document.querySelector(".last_line").classList.add("dark");
}
